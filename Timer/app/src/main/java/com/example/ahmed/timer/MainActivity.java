package com.example.ahmed.timer;

import android.content.Intent;
import android.provider.AlarmClock;
import android.support.design.widget.FloatingActionButton;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.NumberPicker;

import java.util.Calendar;

public class MainActivity extends AppCompatActivity {

    private NumberPicker hourNumberPicker;
    private NumberPicker minutesNumberPicker;
    private FloatingActionButton ftb;

    private int selectedHours;
    private int selectedMinutes;

    private String message = "test";

    private Calendar rightNow;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        hourNumberPicker = (NumberPicker) findViewById(R.id.hoursnp);
        minutesNumberPicker = findViewById(R.id.minsnp);
        ftb = findViewById(R.id.ftb);

        //Populate NumberPicker values from minimum and maximum value range
        //Set the minimum value of NumberPicker
        hourNumberPicker.setMinValue(0);
        minutesNumberPicker.setMinValue(0);
        //Specify the maximum value/number of NumberPicker
        hourNumberPicker.setMaxValue(24);
        minutesNumberPicker.setMaxValue(60);

        //Gets whether the selector wheel wraps when reaching the min/max value.
        hourNumberPicker.setWrapSelectorWheel(true);
        minutesNumberPicker.setWrapSelectorWheel(true);

        //Set a value change listener for NumberPicker
        hourNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                selectedHours = newVal;
            }
        });

        //Set a value change listener for NumberPicker
        minutesNumberPicker.setOnValueChangedListener(new NumberPicker.OnValueChangeListener() {
            @Override
            public void onValueChange(NumberPicker picker, int oldVal, int newVal){
                //Display the newly selected number from picker
                selectedMinutes = newVal;
            }
        });

        ftb.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setTime();
                createAlarm();
            }
        });
    }

    private void setTime(){
        rightNow = Calendar.getInstance();
        this.rightNow.add(Calendar.MINUTE , this.selectedMinutes);
        this.rightNow.add(Calendar.HOUR_OF_DAY, this.selectedHours);
        Log.d("hours" , String.valueOf(this.rightNow.get(Calendar.HOUR_OF_DAY)));
        Log.d("minutes" , String.valueOf(this.rightNow.get(Calendar.MINUTE)));
    }

    private void createAlarm() {
        Intent intent = new Intent(AlarmClock.ACTION_SET_ALARM)
                .putExtra(AlarmClock.EXTRA_MESSAGE, this.message)
                .putExtra(AlarmClock.EXTRA_HOUR, this.rightNow.get(Calendar.HOUR_OF_DAY))
                .putExtra(AlarmClock.EXTRA_MINUTES, this.rightNow.get(Calendar.MINUTE)+1);
        if (intent.resolveActivity(getPackageManager()) != null) {
            startActivity(intent);
        }
    }
}
